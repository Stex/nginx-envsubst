ARG NGINX_VERSION=1.25
FROM nginx:$NGINX_VERSION

COPY ./docker/nginx.conf /etc/nginx/nginx.conf
COPY ./docker/templates /etc/nginx/templates

COPY ./docker/40-html-envsubst.sh /docker-entrypoint.d

ARG BUILD_VERSION=1.0.0
ENV BUILD_VERSION=${BUILD_VERSION}
ENV ENVSUBST_VARS='${BUILD_VERSION}'
