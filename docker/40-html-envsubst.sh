#!/usr/bin/env bash

shopt -s globstar nullglob dotglob

for ext in html js css; do
    for f in /usr/share/nginx/html/**/*.$ext; do
        envsubst "${ENVSUBST_VARS}" < "$f" > "${f}.subst"
        mv "${f}.subst" "${f}"
    done
done
